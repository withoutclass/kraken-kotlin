package com.zablorp.kraken.client

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.zablorp.kraken.client.model.response.AssetInfo
import com.zablorp.kraken.client.model.response.AssetPairInfo
import com.zablorp.kraken.client.model.response.AssetTickerInfo
import com.zablorp.kraken.client.model.response.OHLCData
import com.zablorp.kraken.client.model.response.Result
import com.zablorp.kraken.client.model.response.ServerTime
import com.zablorp.kraken.client.util.buildSignature
import com.zablorp.kraken.client.util.toQueryParams
import okhttp3.FormBody
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import java.time.Instant

private enum class KrakenUrls(val value: String) {
    BASE_URL("https://api.kraken.com"),
    VERSION("/0"),
    PUBLIC("/public"),
    PRIVATE("/private"),
}

private enum class PublicMethods(val method: String) {
    SERVER_TIME("/Time"),
    ASSET_INFO("/Assets"),
    ASSET_PAIRS("/AssetPairs"),
    TICKER("/Ticker"),
    OHLC("/OHLC")
}

private enum class PrivateMethods(val method: String) {
    BALANCE("/Balance")
}

enum class AssetPairInfoType(val value: String) {
    INFO("value"),
    LEVERAGE("leverage"),
    FEES("fees"),
    MARGIN("margin")
}

class KrakenClient(private val apiKey: String, private val secretKey: String) {
    private val okHttpClient = OkHttpClient()
    private val mapper = jacksonObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL)

    companion object {
        val MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8")
    }

    // Public Query Methods

    fun getServerTime(): Result<ServerTime> {
        return mapper.readValue(queryPublic(PublicMethods.SERVER_TIME.method))
    }

    fun getAssetInfo(assetClass: String? = null, assets: List<String>? = null, info: String? = null): Result<Map<String, AssetInfo>> {
        val queryParams = HashMap<String, String>()
        assetClass?.let {
            queryParams.put("aclass", it)
        }

        assets?.let {
            queryParams.put("asset", it.joinToString(","))
        }

        info?.let {
            queryParams.put("value", it)
        }

        return mapper.readValue(queryPublic(PublicMethods.ASSET_INFO.method, queryParams))
    }

    fun getAssetPairs(info: AssetPairInfoType? = null, pairs: List<String>? = null): Result<Map<String, AssetPairInfo>> {
        val queryParams = HashMap<String, String>()

        info?.let {
            queryParams.put("value", it.value)
        }

        pairs?.let {
            queryParams.put("pair", it.joinToString(","))
        }

        return mapper.readValue(queryPublic(PublicMethods.ASSET_PAIRS.method, queryParams))
    }

    fun getTickerInfo(pairs: List<String>? = null): Result<Map<String, AssetTickerInfo>> {
        val queryParams = HashMap<String, String>()

        pairs?.let {
            queryParams.put("pair", it.joinToString(","))
        }

        return mapper.readValue(queryPublic(PublicMethods.TICKER.method, queryParams))
    }

    fun getOHLCData(pair: String, interval: Int? = null, since: String? = null): Result<OHLCData> {
        val queryParams = HashMap<String, String>()

        queryParams.put("pair", pair)

        interval?.let {
            queryParams.put("interval", it.toString())
        }

        since?.let {
            queryParams.put("since", it)
        }

        val result: Result<JsonNode> = mapper.readValue(queryPublic(PublicMethods.OHLC.method, queryParams))
        val pairData = mapper.readValue<List<List<String>>>(result.result!![pair].toString())

        return Result(result.error, OHLCData(hashMapOf(pair to pairData), result.result.get("last").asLong()))
    }

    // Private Query Methods

    fun getAccountBalance(): Result<Map<String, String>> {
        return mapper.readValue(queryPrivate(PrivateMethods.BALANCE.method))
    }

    // Base Query Methods

    @Throws(RuntimeException::class)
    private fun queryPublic(method: String, queryMap: HashMap<String, String>? = null): String {
        val queryString = queryMap?.toQueryParams() ?: ""
        val url = "${KrakenUrls.BASE_URL.value}${KrakenUrls.VERSION.value}${KrakenUrls.PUBLIC.value}$method?$queryString"
        val request = Request.Builder()
                .url(url)
                .addHeader("API-Key", apiKey)
                .build()

        val response = okHttpClient.newCall(request).execute()
        val responseBody = response.body()!!

        return responseBody.string()
    }

    @Throws(RuntimeException::class)
    private fun queryPrivate(method: String, postParams: HashMap<String, String>? = null): String {
        val nonce = generateNonce()
        val reqString = buildReqString(nonce, postParams)
        val formBodyBuilder = FormBody.Builder()
                .add("nonce", nonce)

        postParams?.let { it.iterator().forEach { formBodyBuilder.add(it.key, it.value) } }
        val path = "${KrakenUrls.VERSION.value}${KrakenUrls.PRIVATE.value}$method"
        val address = "${KrakenUrls.BASE_URL.value}$path"

        val request = Request.Builder()
                .url(address)
                .post(formBodyBuilder.build())
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("API-Key", apiKey)
                .addHeader("API-Sign",
                        buildSignature(path = path,
                                secretKey = secretKey,
                                postData = reqString,
                                nonce = nonce))
                .build()

        val response = okHttpClient.newCall(request).execute()
        val responseBody = response.body()!!
        return responseBody.string()
    }

    private fun buildReqString(nonce: String, postParams: HashMap<String, String>? = null): String {
        val noncePart = "nonce=$nonce"
        return if (postParams != null) {
            noncePart + postParams.map { "&${it.key}=${it.value}" }
        } else {
            noncePart
        }
    }

    private fun generateNonce(): String {
        val time = Instant.now().toEpochMilli()
        return time.toString()
    }
}
