package com.zablorp.kraken.client.util

import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.util.Base64
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

fun buildSignature(path: String, secretKey: String, postData: String, nonce: String): String {
    val encodedPath = path.toByteArray()
    return doSHA512Portion(doSHA256Portion(nonce, postData, encodedPath), secretKey).toString(StandardCharsets.UTF_8)
}


fun doSHA256Portion(nonce: String, postData: String, encodedPath: ByteArray): ByteArray {
    val digest = MessageDigest.getInstance("SHA-256")
    val np = nonce + postData
    val encoded = np.toByteArray()
    return encodedPath + digest.digest(encoded)
}

fun doSHA512Portion(sha256Portion: ByteArray, secretKey: String): ByteArray {
    val HMAC_SHA512 = "HmacSHA512"
    val mac = Mac.getInstance(HMAC_SHA512)
    val key: ByteArray = Base64.getDecoder().decode(secretKey)
    val keySpec = SecretKeySpec(key, HMAC_SHA512)
    mac.init(keySpec)
    val signatureBytes = mac.doFinal(sha256Portion)
    return Base64.getEncoder().encode(signatureBytes)
}
