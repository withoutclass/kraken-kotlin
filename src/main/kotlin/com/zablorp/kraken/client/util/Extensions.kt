package com.zablorp.kraken.client.util

fun HashMap<String, String>.toQueryParams(): String = this.map { "${it.key}=${it.value}" }.joinToString("&")