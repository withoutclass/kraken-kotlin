package com.zablorp.kraken.client.model.response

import com.fasterxml.jackson.annotation.JsonAlias

data class AssetTickerInfo(@JsonAlias("a") val ask: List<String>,
                           @JsonAlias("b") val bid: List<String>,
                           @JsonAlias("c") val lastTradeClosed: List<String>,
                           @JsonAlias("v") val volume: List<String>,
                           @JsonAlias("p") val volumeWeightedAveragePriceToday: List<String>,
                           @JsonAlias("t") val numberOfTradesToday: List<String>,
                           @JsonAlias("l") val low: List<String>,
                           @JsonAlias("h") val high: List<String>,
                           @JsonAlias("o") val openingPriceToday: String)