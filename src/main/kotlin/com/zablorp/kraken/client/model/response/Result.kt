package com.zablorp.kraken.client.model.response

data class Result<out T>(val error: List<String>,
                         val result: T?)
