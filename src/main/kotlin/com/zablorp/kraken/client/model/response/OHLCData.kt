package com.zablorp.kraken.client.model.response

import com.fasterxml.jackson.annotation.JsonAnySetter

data class OHLCData(var pairData: HashMap<String, List<List<String>>>,
                    val last: Long)