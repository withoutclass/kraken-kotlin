package com.zablorp.kraken.client.model

interface IQueryParam {
    fun toUrlEncodedQueryString(): String
}
