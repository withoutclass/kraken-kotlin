package com.zablorp.kraken.client.model.response

data class ServerTime(val unixtime: Long,
                      val rfc1123: String)
