package com.zablorp.kraken.client.model.response

import java.math.BigDecimal

data class AssetPairInfo(val altname: String,
                         val aclass_base: String,
                         val base: String,
                         val aclass_quote: String,
                         val quote: String,
                         val lot: String,
                         val pair_decimals: BigDecimal,
                         val lot_decimals: BigDecimal,
                         val lot_multiplier: Int,
                         val leverage_buy: List<String>,
                         val leverage_sell: List<String>,
                         val fees: List<List<BigDecimal>>,
                         val fees_maker: List<List<BigDecimal>>,
                         val fee_volume_currency: String,
                         val margin_call: BigDecimal,
                         val margin_stop: BigDecimal)