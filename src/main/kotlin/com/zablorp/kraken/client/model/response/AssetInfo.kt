package com.zablorp.kraken.client.model.response

import com.fasterxml.jackson.annotation.JsonAlias

data class AssetInfo(val aclass: String,
                     val altname: String,
                     val decimals: Int,
                     @JsonAlias("display_decimals") val displayDecimals: Int)
