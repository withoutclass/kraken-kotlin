package com.zablorp.kraken.client.model.request

import com.zablorp.kraken.client.model.IQueryParam
import java.net.URLEncoder

data class BasePostBody(val nonce: String,
                        val otp: String?) : IQueryParam {

    override fun toUrlEncodedQueryString(): String {
        val queryString = "nonce=$nonce"
        otp?.let {
            queryString.plus("&otp=$otp")
        }

        return URLEncoder.encode(queryString, "UTF-8")
    }

}
