package com.zablorp.kraken

import com.zablorp.kraken.client.KrakenClient
import com.zablorp.kraken.client.model.response.AssetInfo
import com.zablorp.kraken.client.model.response.OHLCData
import org.testng.Assert.assertEquals
import org.testng.Assert.assertTrue
import org.testng.annotations.Test
import java.time.Instant

class PublicQueryClientTests {

    private val krakenClient: KrakenClient = KrakenClient(System.getenv("api.key"), System.getenv("secret.key"))

    @Test
    fun testGetServerTime() {
        val response = krakenClient.getServerTime()

        val instantFromUnix = Instant.ofEpochSecond(response.result!!.unixtime)
        assertTrue(instantFromUnix.isBefore(Instant.now().plusSeconds(15)))
    }

    @Test
    fun testGetAssetInfo() {
        val response = krakenClient.getAssetInfo(assets = listOf("BCH"))

        assertTrue(response.result != null)
        assertTrue(response.result!!.isNotEmpty())
        val assetInfo: AssetInfo = response.result!!["BCH"]!!
        assertTrue(assetInfo.aclass == "currency")
        assertTrue(assetInfo.altname == "BCH")
    }

    @Test
    fun testGetAssetPairs() {
        val response = krakenClient.getAssetPairs(pairs = listOf("BCHUSD"))
        val result = response.result!!
        val assetPairInfo = result["BCHUSD"]!!
        assertEquals("BCHUSD", assetPairInfo.altname)
        assertEquals("ZUSD", assetPairInfo.quote)
        assertTrue(assetPairInfo.fees.isNotEmpty())
        assertTrue(assetPairInfo.fees_maker.isNotEmpty())
    }

    @Test
    fun testGetTickerInfo() {
        val response = krakenClient.getTickerInfo(listOf("BCHUSD"))
        val tickerInfo = response.result!!["BCHUSD"]!!
        assertEquals(3, tickerInfo.ask.size)
        assertEquals(3, tickerInfo.bid.size)
        assertEquals(2, tickerInfo.lastTradeClosed.size)
        assertEquals(2, tickerInfo.volume.size)
        assertEquals(2, tickerInfo.volumeWeightedAveragePriceToday.size)
        assertEquals(2, tickerInfo.numberOfTradesToday.size)
        assertEquals(2, tickerInfo.low.size)
        assertEquals(2, tickerInfo.high.size)
    }

    @Test
    fun testGetOHLCData() {
        val response = krakenClient.getOHLCData(pair = "BCHUSD")
        assertTrue(response.result != null)
    }
}
