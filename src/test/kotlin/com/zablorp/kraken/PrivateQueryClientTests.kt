package com.zablorp.kraken

import com.zablorp.kraken.client.KrakenClient
import org.testng.Assert
import org.testng.annotations.Test

class PrivateQueryClientTests() {

    private val krakenClient: KrakenClient = KrakenClient(System.getenv("api.key"), System.getenv("secret.key"))

    @Test
    fun testGetAccountBalance() {
        val response = krakenClient.getAccountBalance()
        Assert.assertTrue(response.error.isEmpty())
        Assert.assertTrue(response.result != null)
    }
}