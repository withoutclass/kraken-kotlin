import com.beust.kobalt.buildScript
import com.beust.kobalt.file
import com.beust.kobalt.plugin.packaging.assemble
import com.beust.kobalt.project
import com.beust.kobalt.test

object versions {
    val okhttp = "3.8.1"
    val kotlin = "1.1.60"
    val jackson = "2.9.0"
    val testNG = "6.11"
}

val bs = buildScript {
}

val p = project {
    name = "kraken-kotlin"
    group = "com.zablorp"
    artifactId = name
    version = "0.1"

    sourceDirectories {
        path("src/main")
    }
    sourceDirectoriesTest {
        path("src/test")
    }

    dependencies {
        compile("org.jetbrains.kotlin:kotlin-runtime:${versions.kotlin}")
        compile("org.jetbrains.kotlin:kotlin-stdlib:${versions.kotlin}")
        compile("com.squareup.okhttp3:okhttp:${versions.okhttp}")
        compile("com.fasterxml.jackson.module:jackson-module-kotlin:${versions.jackson}")
    }

    dependenciesTest {
        compile("org.testng:testng:${versions.testNG}")
    }

    assemble {
        jar {
        }
    }

    test {
        args("src/test/resources/testng.xml")
    }

}
